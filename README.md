# Startup name generator in Flutter

## Getting Started

For help getting started with Flutter, view our online [documentation](https://flutter.io/).

Implement a simple mobile app using Flutter that generates proposed names for a startup company. The user can select and unselect names, saving the best ones. The code lazily generates ten names at a time.